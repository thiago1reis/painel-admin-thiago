# Congelado Painel Admin Laravel

![capa](https://nanoincub.com.br/wp-content/uploads/2019/09/cropped-nano-incub-desenvolvimento-plataformas-digitais-logo.png)

![PHP Version](https://img.shields.io/badge/PHP-8.1-brightgreen) ![Node LTS](https://img.shields.io/badge/node-%3E%3D%2016.17.0-brightgreen) ![Composer(https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos) ](https://img.shields.io/badge/composer-^2.3.0-brightgreen) ![by Nano](https://img.shields.io/badge/%20by-Nano_Incub-informational?color=ee564b)

[🛠 Ferramentas](#ferramentas) | [⚙ Instalação](#instalação)

---

## Ferramentas

- Requisitos
  - [Node](https://nodejs.org/en/)
  - [NPM](https://www.npmjs.com/)
  - [Composer](https://getcomposer.org/)
  - [Git](https://git-scm.com/downloads)
  - [Yarn](https://yarnpkg.com/)

- Recomendados
  - [Docker](https://www.docker.com/)
  - [Container MySQL](https://gitlab.com/nanoincub/docker/docker-mysql)
  - [Container PHP & Apache](https://gitlab.com/nanoincub/docker/docker-php-apache)

---

## Instalação

### Clonar o repositório
```bash
  git clone https://gitlab.com/nanoincub/congelados/painel-admin-laravel.git
```

### Baixar as dependências
```bash
  cd <projeto>
  composer install
```

### Compilar o projeto
```bash
  yarn
  yarn run dev
```

### Banco de dados e .env
Crie um novo banco de dados usando o seu gerenciador de preferência (ex: phpMyAdmin).

Copie o arquivo .env.example e renomeie-o para apenas .env, este arquivo contém as variáveis de ambiente.

Faça as alterações necessárias nas seguintes linhas do arquivo .env para configurar o banco de dados

```bash
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=laravel
  DB_USERNAME=root
  DB_PASSWORD=
```
Exemplo:
```bash
  DB_CONNECTION=mysql
  DB_HOST=mysql
  DB_PORT=3306
  DB_DATABASE=painel_admin_laravel
  DB_USERNAME=root
  DB_PASSWORD=123
```

Altere também as variáveis `APP_URL` e `ASSET_URL` para o endereço que será utilizado no navegador para acessar o painel. Utilize HTTP ao invés de HTTPS pois o segundo está quebrando a estilização.

Por padrão, o arquivo .env vem com o `SESSION_DRIVER` definido para _database_, isso poderá ser alterado para _file_, de acordo com as necessidades da aplicação sendo desenvolvida.

### Comandos do Laravel
Antes de acessar o painel, deve-se rodar dois comandos no diretório raiz do projeto
```bash
  php artisan key:generate
  php artisan migrate
```
O primeiro comando gerará a chave da aplicação que é inserida no arquivo .env, o segundo irá criar as tabelas e colunas no banco de dados.

- Opcional: você pode executar o comando `php artisan db:seed` para inserir dados de teste no banco de dados.

### Erro de permissão
É possível que a seguinte mensagem apareça ao tentar acessar o painel
`The stream or file "/app/caule-app/admin/storage/logs/laravel.log" could not be opened in append mode: Failed to open stream: Permission denied`
Neste caso, utilize o comando `chown` e altere o dono da pasta __storage__ para $USER:www-data.

### Acessando o painel
O painel possui um usuário super administrador, as credenciais são as seguintes:
> Usuário: desenvolvimento@nanoincub.com.br
> Senha: 123456789
