<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserRequest;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
  use LivewireAlert;

  public function index()
  {
    $admins = Admin::all();
    $roles = Role::get();
    $userCount = $admins->count();
    $verified = Admin::whereNotNull('email_verified_at')->get()->count();
    $notVerified = Admin::whereNull('email_verified_at')->get()->count();
    $adminsUnique = $admins->unique(['email']);
    $userDuplicates = $admins->diff($adminsUnique)->count();

    $admins = $admins->map(function ($item) {
      $color_status = match ($item->status) {
        'ativo' => 'bg-label-success',
        'pendente' => 'bg-label-info',
        'desativado' => 'bg-label-danger',
        default => 'bg-label-primary'
      };

      $item->color_status = $color_status;
      return $item;
    })->paginate(15);

    return view('pages.admin.admin-list', [
      'totalUser' => $userCount,
      'verified' => $verified,
      'notVerified' => $notVerified,
      'userDuplicates' => $userDuplicates,
      'admins' => $admins,
      'roles' => $roles
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(UserRequest $request)
  {
    // create new one if email is unique
    $userEmail = Admin::where('email', $request->email)->first();

    if (empty($userEmail)) {
      $user = Admin::create(
        ['name' => $request->name, 'email' => $request->email, 'password' => bcrypt($request->password), 'status' => $request->status]
      );

      // sync role
      $user->assignRole($request->role);

      // user created
      $this->flash('success', 'Usuário Cadastrado com Sucesso!');
      return redirect()->back();
    } else {

      // user already exist
      $this->flash('danger', 'Usuário já existe!');
      return response()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $where = ['id' => $id];

    $admins = User::where($where)->first();

    return response()->json($admins);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $userID = $id;

    $admins = User::updateOrCreate(
      ['id' => $userID],
      [
        'name' => $request->name,
        'email' => $request->email,
        'status' => $request->status,
      ]
    );

    // user updated
    $this->flash('success', 'Usuário Editado com Sucesso!');
    return redirect()->back();
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Admin $admin)
  {
    if ($admin->id === auth()->user()->id) {
      // Error delete current admin
      $this->flash('error', 'Você não pode excluir seu usuário', [
        'timerProgressBar' => true,
      ]);
      return redirect()->back();
    }

    // admin deleted
    $admin->delete();

    $this->flash('success', 'Administrador removido com Sucesso!');
    return redirect()->back();
  }
}
