<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Services\CreateRoleService;
use Illuminate\Http\Request;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
  use LivewireAlert;

  public Role $_role;
  private CreateRoleService $createRoleService;

  public function __construct(CreateRoleService $createRoleService)
  {
    $this->_role = new Role();
    $this->createRoleService = $createRoleService;
  }


  public function index()
  {
    $roles = Role::paginate(15);
    $modules = Module::get();

    return view('pages.permissao.permissao-list', ['roles' => $roles, 'modules' => $modules]);
  }

  public function store(Request $request)
  {
    $this->_role->name = $request->modalRoleName;
    $this->createRoleService->execute($this->_role);
    $this->_role->permissions()->sync($request->permissions);

    return redirect()->back();
  }

  public function destroy(Role $role)
  {
    if ($role->id === 1) {
      $this->flash('error', 'Você não pode deletar o Super Administrador', [
        'timerProgressBar' => true,
      ]);
      return redirect()->back();
    }

    $role->delete();
    $this->flash('success', 'Permissão removida com sucesso!');

    return redirect()->back();
  }
}
