<?php

namespace App\Http\Livewire\Manager;

use App\Models\Admin;
use Livewire\Component;
use Spatie\Permission\Models\Role;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class AdminManager extends Component
{
  use LivewireAlert;

  public Admin $admin;
  public $adminRole;
  public $password;
  public $password_confirmation;

  public $roles;

  protected $rules = [
    'admin.name' => 'required',
    'admin.email' => 'required',
    'password' => 'nullable',
    'password_confirmation' => 'nullable|same:password',
    'admin.status' => 'required'
  ];

  public function mount(Admin $admin)
  {
    $this->admin = $admin;
    $this->roles = Role::get();
    $this->adminRole = $admin->roles->pluck('name');
  }

  public function update()
  {

    if ($this->password) {
      $this->admin->password =  bcrypt($this->password);
    }

    // Validate
    $this->validate();

    $this->admin->save();
    $this->admin->syncRoles($this->adminRole);

    // admin updated
    $this->flash('success', 'Dados salvo com sucesso!', [], request()->header('Referer'));
  }

  public function render()
  {
    if ($this->admin->id) {
      $this->title = 'Editar administrador';
    }
    $this->title = 'Cadastrar administrador';

    return view('pages.admin.admin-modal-form');
  }
}
