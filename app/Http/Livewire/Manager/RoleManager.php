<?php

namespace App\Http\Livewire\Manager;

use App\Models\Module;
use App\Services\CreateRoleService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleManager extends Component
{
  use LivewireAlert;

  public $type;
  public $roles;
  public $modules;
  public Role $role;
  public $permissions = [];
  public $checkboxSelectAll;

  private CreateRoleService $createRoleService;

  // protected $messages = [
  //   'require.*' => 
  // ]

  protected $rules = [];

  public function boot(CreateRoleService $createRoleService)
  {
    $this->rules = [
      'role.name' => ['required', Rule::unique(Role::class)]
    ];

    $this->createRoleService = $createRoleService;
  }

  public function mount(Role $role, $type = 'edit')
  {
    $this->modules = Module::get();
    $this->roles = Role::get();
    $this->role = $role;
    $this->type = $type;

    $_permissions = $role->permissions;
    $_permissions = $_permissions->map(fn ($item) => $item->id);
    $this->permissions = $_permissions->toArray();
  }

  public function submit()
  {
    // Validate
    $this->validate();

    $this->role->guard_name = 'web';
    $this->createRoleService->execute($this->role);
    $this->role->permissions()->sync($this->permissions);

    $this->emit('hideModal');
    $this->flash('success', 'Permissão cadastrada com sucesso!', [], request()->header('Referer'));
  }

  public function update()
  {
    // Validate
    $this->validate([
      'role.name' => ['required', Rule::unique(Role::class, 'name')->ignore($this->role->id)]
    ]);

    if ($this->role->id === 1) {
      return $this->flash('error', 'Você não pode alterar o Super Administrador', [
        'timerProgressBar' => true,
      ], route('permissao.index'));
    }

    $this->role->guard_name = 'web';
    $this->role->save();
    $this->role->permissions()->sync($this->permissions);

    $this->emit('hideModal');
    $this->flash('success', 'Permissão editada com sucesso!', [], request()->header('Referer'));
  }

  public function UpdatedCheckboxSelectAll()
  {
    if ($this->checkboxSelectAll) {
      $all_permissions = Permission::get('id');
      $this->permissions  = $all_permissions->map(fn ($item) => $item->id)->toArray();
    } else {
      $this->permissions = [];
    }
  }

  public function render()
  {
    if ($this->type === 'view') {
      $this->title = 'Visualizar permissão';
    } else {
      if ($this->role->id) {
        $this->title = 'Editar permissão';
      } else {
        $this->title = 'Cadastrar permissão';
      }
    }
    return view('pages.permissao.permissao-modal-form');
  }
}
