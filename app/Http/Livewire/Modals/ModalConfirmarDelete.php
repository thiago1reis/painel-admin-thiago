<?php

namespace App\Http\Livewire\Modals;

use Livewire\Component;

class ModalConfirmarDelete extends Component
{
    public $string;

    //Recebe a string contendo a URL a ser utilizada para exclusão
    public function mount($string)
    {
      $this->string = $string;
    }

    public function render()
    {
        return view('pages._partials._modals.modal-confirmar-delete');
    }
}
