<?php

namespace App\Http\Requests\Auth;

use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', Rule::unique(Admin::class, 'email')->ignore($this->admin)],
      'password' => [
        'confirmed',
        Rule::requiredIf(fn () => !$this->admin),
        Rule::when($this->admin, 'nullable'),
      ],
      'status' => 'required',
      'role' => 'required'
    ];
  }
}
