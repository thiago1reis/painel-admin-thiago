<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'movement';
    
    protected $fillable = [
      'movement_type',
      'value',
      'note',
      'employee_id',
      'administrator_id',
    ];

    
    public static function typesMovement()
    {
      return collect([
        'income' => 'Income',
        'expense' => 'Expense',
      ]);
    }
}
