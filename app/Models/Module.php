<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class Module extends Model
{
  protected $table = 'admin_module';

  const MODULE_ADMIN = 1;
  const MODULE_PERMISSAO = 2;

  public function permissions()
  {
    return $this->hasMany(Permission::class, 'admin_module_id', 'id');
  }
}
