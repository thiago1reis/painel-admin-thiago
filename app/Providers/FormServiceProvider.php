<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormFacade as Form;

class FormServiceProvider extends ServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    Form::component('bsText', 'components.form.text', ['name', 'value' => null, 'attributes' => []]);
    Form::component('bsSelect', 'components.form.select', ['name', 'value', 'options' => [], 'attributes' => []]);
  }
}
