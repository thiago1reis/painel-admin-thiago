<?php

namespace App\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;

class BuscaModelService
{
  public function execute(Model $model, array $parameters, $numberPerPage): LengthAwarePaginator
  {
    return $model
      ->where(function ($query) use ($parameters) {
        foreach ($parameters as $field => $value) {
          $query->where($field, 'like', "%{$value}%");
        }
      })
      ->orderBy('id', 'desc')
      ->paginate($numberPerPage);
  }
}
