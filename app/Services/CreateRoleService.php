<?php

namespace App\Services;

use Spatie\Permission\Models\Role;

class CreateRoleService
{
  /**
   *
   * @param Role $role
   * @return void
   */
  public function execute(Role &$role)
  {
    $role->save();
  }
}
