<?php

use App\Models\Module;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

  private array $itens = [
    [
      'name' => 'Administradores',
    ],
    [
      'name' => 'Permissões'
    ]
  ];
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Module::insert($this->itens);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('modules', function (Blueprint $table) {
      //
    });
  }
};
