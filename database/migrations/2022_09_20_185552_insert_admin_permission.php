<?php

use App\Models\Module;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

return new class extends Migration
{
  private array $itens = [
    [
      'name' => 'usuario-read',
      'admin_module_id' => Module::MODULE_ADMIN,
      'function_name' => 'Ler',
      'guard_name' => 'web'
    ],
    [
      'name' => 'usuario-edit',
      'admin_module_id' => Module::MODULE_ADMIN,
      'function_name' => 'Editar',
      'guard_name' => 'web'
    ],
    [
      'name' => 'usuario-create',
      'admin_module_id' => Module::MODULE_ADMIN,
      'function_name' => 'Cadastro',
      'guard_name' => 'web'
    ],
    // Module Rule
    [
      'name' => 'permissao-read',
      'admin_module_id' => Module::MODULE_PERMISSAO,
      'function_name' => 'Ler',
      'guard_name' => 'web',
    ],
    [
      'name' => 'permissao-edit',
      'admin_module_id' => Module::MODULE_PERMISSAO,
      'function_name' => 'Editar',
      'guard_name' => 'web'
    ],
    [
      'name' => 'permissao-create',
      'admin_module_id' => Module::MODULE_PERMISSAO,
      'function_name' => 'Cadastro',
      'guard_name' => 'web'
    ]
  ];

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Permission::insert($this->itens);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('permissions', function (Blueprint $table) {
      //
    });
  }
};
