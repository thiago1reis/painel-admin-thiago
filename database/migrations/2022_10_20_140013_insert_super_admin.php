<?php

use App\Models\Admin;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      $superadmin =
      [
        'id' => 1,
        'name' => 'Super Administrador',
        'email' => 'desenvolvimento@nanoincub.com.br',
        'email_verified_at' => now(),
        'password' => bcrypt('123456789'),
        'status' => 'ativo',
        'remember_token' => Str::random(10),
      ];

      $role = Role::create([
        'name' => 'Super Admin',
        'guard_name' => 'web'
      ]);
      $permissions = Permission::all();
      $role->permissions()->sync($permissions);
      Admin::insert($superadmin);
      Admin::first()->assignRole($role);
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
