<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {

    $role1 = Role::create([
      'name' => 'Administrador',
      'guard_name' => 'web'
    ]);

    $role2 = Role::create([
      'name' => 'Gestor',
      'guard_name' => 'web'
    ]);

    $permissions = Permission::all();

    $role1->permissions()->sync($permissions);
    $role2->permissions()->sync($permissions);

    $admins = Admin::factory(25)->create();
    $role1->users()->attach($admins);

    $admins = Admin::factory(25)->create();
    $role2->users()->attach($admins);
  }
}
