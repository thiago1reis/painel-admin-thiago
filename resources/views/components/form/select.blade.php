{{ Form::label($name, null, ['class' => 'control-label']) }}
{{ Form::select($name, $options, $value, array_merge(['class' => 'form-select'], $attributes)) }}
