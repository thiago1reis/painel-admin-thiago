<div class="modal-dialog modal-lg modal-simple modal-dialog-centered modal-add-new-role" role="document">
    <div class="p-3 modal-content p-md-5">
        <div class="modal-body">
            <div class="mb-4 text-center">
                <h3 class="modal-title" id="exampleModalLabel">Deseja realmente deletar?</h3>
            </div>
            <div class="d-flex gap-3">
                <form method="POST" action="{{ $string }}" class="d-grid w-100">
                    @csrf
                    @method('DELETE')
                    <button class='btn btn-primary' type="submit" alt="Deletar">Deletar</button>
                </form>
                <button type="button" class="btn btn-label-secondary d-grid w-100" data-bs-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
