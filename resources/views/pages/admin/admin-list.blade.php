@extends('layouts/layoutMaster')

@section('title', 'Usuários')

@section('vendor-style')

@endsection

@section('vendor-script')

@endsection

@section('content')

    <div class="d-flex">
        <div class="flex-fill">
            <h4 class="py-3 mb-2 fw-bold">Lista de administradores</h4>
            <p>
                Lista de administradores que poderão acessar o painel admin.
            </p>
        </div>
        <div class="align-self-end mb-3">
          @can('usuario-create')
            <button onclick="Livewire.emit('showModal', 'manager.admin-manager')"
                class="btn btn-primary text-nowrap">Adicionar</button>
          @endcan
        </div>
    </div>

    <!-- Users List Table -->
    <div class="card">
        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Tipo de permissão</th>
                        <th>Status</th>
                        @can('usuario-edit')
                        <th>Actions</th>
                        @endcan
                    </tr>
                </thead>
                <tbody>

                    @foreach ($admins as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->getRoleNames()->first() }}</td>
                            <td><span class="badge {{ $item->color_status }} me-1">{{ $item->status }}</span></td>
                            @can('usuario-edit')
                            <td>
                                <div>
                                    <button class="btn p-0"
                                        onclick="Livewire.emit('showModal', 'manager.admin-manager', {{ $item->id }})"><i
                                            class="bx bx-edit-alt"></i></button>
                                    <button class="btn p-0"
                                        onclick="Livewire.emit('showModal', 'modals.modal-confirmar-delete', '{{ route('administrador.destroy', ['admin' => $item->id]) }}')">
                                        <i class="bx bx-trash"></i></button>
                                </div>
                            </td>
                            @endcan
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="m-2">
            {{ $admins->links() }}
        </div>
        <!-- Offcanvas to add new user -->

    </div>
@endsection
