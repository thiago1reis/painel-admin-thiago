<div id="addRoleModal" class="modal-dialog modal-lg modal-simple modal-dialog-centered modal-add-new-role" tabindex="-1"
    aria-hidden="true">
    <div class="p-3 modal-content p-md-5">
        <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="mb-4 text-center">
                <h3 class="role-title">{{ $title }}</h3>
            </div>

            <!-- Add role form -->
            <form wire:submit.prevent="update" id="addRoleForm" class="row g-3">

                <div class="mb-3">
                    <label for="name" class="form-label">Nome</label>
                    <input wire:model="admin.name" name="name" type="text"
                        class="form-control @error('name') is-invalid @enderror" />

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="defaultFormControlInput" class="form-label">Email</label>
                    <input wire:model="admin.email" type="text" name="email"
                        class="form-control @error('email') is-invalid @enderror" />

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-password-toggle mb-3">
                    <label class="form-label" for="password">Senha</label>
                    <div wire:ignore class="input-group">
                        <input wire:model="password" type="password" name="password"
                            class="form-control @error('password') is-invalid @enderror" id="basic-default-password12"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="basic-default-password" />
                        <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>

                </div>

                <div class="form-password-toggle mb-3">
                    <label class="form-label" for="password_confirmation">Confirmar Senha</label>
                    <div class="input-group">
                        <input wire:model.lazy="password_confirmation" type="password" name="password_confirmation"
                            class="form-control @error('password_confirmation') is-invalid @enderror"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="basic-default-password" />
                        <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>

                        @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="mb-3">
                    <label for="status" class="form-label">Status</label>
                    <select wire:model="admin.status" class="form-select @error('status') is-invalid @enderror"
                        name="status">
                        <option selected value="">Selecione ...</option>
                        <option value="ativo">Ativo</option>
                        <option value="bloqueado">Bloqueado</option>
                        <option value="desativado">Desativado</option>
                    </select>

                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="role" class="form-label">Permissão</label>
                    <select wire:model="adminRole" class="form-select @error('role') is-invalid @enderror"
                        name="role">
                        <option selected value="0">Selecione ...</option>
                        @foreach ($roles as $item)
                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                        @endforeach
                    </select>

                    @error('role')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="d-flex gap-3">
                    <button type="submit" class="btn btn-primary d-grid w-100">Salvar</button>
                    <button type="button" class="btn btn-label-secondary d-grid w-100" data-bs-dismiss="modal"
                        aria-label="Close">Cancelar</button>
                </div>

            </form>
            <!--/ Add role form -->
        </div>
    </div>
</div>

</div>

<!--/ Add Role Modal -->
