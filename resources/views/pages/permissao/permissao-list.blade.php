@php
$configData = Helper::appClasses();
@endphp

@extends('layouts/layoutMaster')

@section('title', 'Permissões')

@section('vendor-style')
    {{--  --}}
@endsection

@section('vendor-script')
    {{--  --}}
@endsection

@section('page-script')

@endsection

@section('content')

    <div class="d-flex">
        <div class="flex-fill">
            <h4 class="py-3 mb-2 fw-bold">Lista de permissões</h4>

            <p>
                Lista de tipo de permissões que podem ser vinculados aos administradores.
            </p>
        </div>
        <div class="align-self-end mb-3">
          @can('permissao-create')
            <button onclick="Livewire.emit('showModal', 'manager.role-manager')"
                class="btn btn-primary text-nowrap">Adicionar</button>
          @endcan
        </div>
    </div>

    <!-- Role dashboard-->
    <div class="row g-4">


        <div class="col-12">
            <!-- Role Table -->
            <div class="card">

                <div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="75%">Nome da permissão</th>
                                {{-- <th width="45%" class="text-center">Total de administradores</th> --}}
                                @can('permissao-edit')
                                <th width="25%">Actions</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($roles as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    
                                    <td>
                                        <div>
                                            <button class="btn p-0"
                                              onclick="Livewire.emit('showModal', 'manager.role-manager', {{ $item->id }}, 'view')"><i
                                                  class="fa-regular fa-eye"></i></button>
                                                  @can('permissao-edit')
                                          @if($item->id != 1) 
                                            <button class="btn p-0"
                                                onclick="Livewire.emit('showModal', 'manager.role-manager', {{ $item->id }})"><i
                                                    class="bx bx-edit-alt"></i></button>
                                            <button class="btn p-0"
                                                onclick="Livewire.emit('showModal', 'modals.modal-confirmar-delete', '{{ route('permissao.destroy', ['role' => $item->id]) }}')"><i
                                                    class="bx bx-trash"></i></button>
                                          @endif
                                        </div>
                                    </td>
                                    @endcan
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="m-2">
                    {{ $roles->links() }}
                </div>
                <!--/ Role Table -->
            </div>
        </div>
    </div>
    <!--/ Role cards -->

@endsection
