<div id="addRoleModal" class="modal-dialog modal-lg modal-simple modal-dialog-centered modal-add-new-role" tabindex="-1"
    aria-hidden="true">
    <div class="p-3 modal-content p-md-5">
        <div class="modal-body">
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="mb-4 text-center">
                <h3 class="role-title">{{ $title }}</h3>
            </div>

            <!-- Add role form -->
            <form wire:submit.prevent="update" id="addRoleForm" class="row g-3">

                <div class="mb-4 col-12">
                    <label class="form-label" for="modalRoleName">Nome da Permissão</label>
                    <input wire:model="role.name" type="text" id="modalRoleName" name="modalRoleName"
                        class="form-control @error('role.name') 'is-invalid' @enderror"
                        placeholder="Insira um nome da permissão" tabindex="-1" 
                        {{ $type === 'view' ? 'disabled' : '' }} />
                    @error('role.name')
                        <span class="d-block invalid-feedback">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-12">
                    <h4>Permissões</h4>
                    <!-- Permission table -->
                    <div>
                        <table class="table table-flush-spacing">
                            <tbody>
                                @if($type != 'view')
                                <tr>
                                    <td class="text-nowrap fw-semibold">Administrador <i class="bx bx-info-circle bx-xs"
                                            data-bs-toggle="tooltip" data-bs-placement="top"
                                            title="Allows a full access to the system"></i></td>
                                    <td>
                                        <div class="form-check">
                                            <input wire:model="checkboxSelectAll" class="form-check-input"
                                                type="checkbox" id="js-select-all" {{ $type === 'view' ? 'disabled' : '' }} />
                                            <label class="form-check-label" for="js-select-all">
                                                Selecionar Todos
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                @endif

                                @foreach ($modules as $module)
                                    <tr>
                                        <td class="text-nowrap fw-semibold">{{ $module->name }}</td>
                                        <td>
                                            <div class="d-flex">

                                                @foreach ($module->permissions as $idx => $permission)
                                                    <div wire:key="{{ $idx }}" class="form-check me-3 me-lg-5">
                                                        <label class="form-check-label">
                                                            <input wire:model.lazy="permissions"
                                                                class="js-select-only form-check-input" type="checkbox"
                                                                value="{{ $permission->id }}" 
                                                                {{ $type === 'view' ? 'disabled' : '' }} />
                                                            {{ $permission->function_name }}
                                                        </label>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- Permission table -->
                </div>
                
                @if($type != 'view')
                <div class="text-center col-12">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1">Salvar</button>
                    <button type="reset" class="btn btn-label-secondary" data-bs-dismiss="modal"
                        aria-label="Close">Cancelar</button>
                </div>
                @endif
            </form>
            <!--/ Add role form -->
        </div>
    </div>

</div>

<!--/ Add Role Modal -->
