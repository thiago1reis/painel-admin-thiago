<?php

use App\Http\Controllers\Pages\RoleController;
use App\Http\Controllers\Pages\AdminController;
use App\Http\Controllers\Pages\HomePage;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => [
  'auth:sanctum',
  config('jetstream.auth_session'),
  'verified'
]], function () {

  // Checa permissões
  Route::group(['middleware' => ['can:usuario-read']], function () {
    // Users 
    Route::get('/administrador', [AdminController::class, 'index'])->name('administrador.index');
    Route::delete('/administrador/{admin}', [AdminController::class, 'destroy'])->name('administrador.destroy')->middleware('can:usuario-edit');
    Route::post('/administrador', [AdminController::class, 'store'])->name('administrador.store')->middleware('can:usuario-create|usuario-edit');;
  });

  Route::group(['middleware' => ['can:permissao-read']], function () {
    // Roles 
    Route::get('/permissao', [RoleController::class, 'index'])->name('permissao.index');
    Route::post('/permissao', [RoleController::class, 'store'])->name('permissao.store')->middleware('can:permissao-edit|permissao-create');
    Route::delete('/permissao/{role}', [RoleController::class, 'destroy'])->name('permissao.destroy')->middleware('can:permissao-edit');
  });

  // Index
  Route::get('/', [HomePage::class, 'index'])->name('dashboard');
});
